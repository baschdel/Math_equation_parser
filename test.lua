local tree = require("tabletree").tree


dfunc = {}
ifunc = {}

function decode(v,t)
 print("--------------------")
 if not t then t = "default" print("no type specified! using default decoder") end
 print("decoding "..tostring(v).." as "..tostring(t))
 if dfunc[t] then
  local ok,res,r = pcall(dfunc[t],v)
  if ok then 
   if type(res) == "table" then 
    if not res.raw  then res.raw  = v end
    if not res.type then res.type = t end
    return res
   else
    return {raw=v,type=t,error="decoder errored",error_details={r,res}}
   end
  else
   return {raw=v,type=t,error = "decoder failed",error_details={res}}
  end
 else
  print("[error]no decoder registred for type "..tostring(t)) 
  return {raw=v,type=t,error = "no decoder"}
 end
end

function is(t,v)
 print("is this a "..tostring(t).."?")
 print(v)
 res = false
 if ifunc[t] then 
  ok,res = pcall(ifunc[t],v)
  if not ok then
   print("[error][is]["..tostring(t).."]")
   print(r)
   r=res res = nil 
  end
 end
 if res then print(">Yes") else print(">No") end
 return res,r
end
------------------------------------------------------
--utils

function count(str,pattern)
 lc = 0
 c  = 0
 while lc do
  lc = str:find(pattern,lc+1)
  c=c+1
 end
 return c-1 
end

--[[
function hasEqualBrackets(str)
 return count(str,"%(") == count(str,"%)")
end
--]]

function checkBrackets(s)
 local i=1
 local c=0
 while i<=s:len() do
  local x = s:sub(i,i)
  if x==")" then c=c-1 end
  if x=="(" then c=c+1 end
  if c<0 then return false end
  i=i+1
 end
 return c==0
end
-------------------------------------------------------
--values

---numbers
nbases = {
 b = 2,
 o = 8,
 d = 10,
 x = 16,
}

--allowed chars after a string.lower
--these values will be fed into a string.match("["..nallowed[n]..otherallowed.."]")
nallowed = {
 [2]  = "01",
 [8]  = "01234567",
 [10] = "0123456789%.",
 [16] = "0123456789abcdef",
}

---signs
signs = {
"=",
"==",
"+",
"-",
"*",
"/",
"^",
}
--patterns
signsp = {
"=",
"==",
"%+",
"%-",
"%*",
"/",
"%^",
}

-------------------------------------------------------
--testers

function isnumber(v)
 v = v:lower()
 --print("isnumber",v)
 bk = v:sub(1,2):match("0(.)")
 --print("bk",bk)
 if bk then 
  --print(bk:match("["..nallowed[10].."]+"),v:match("["..nallowed[10].."]+"), v)
  if bk:match("["..nallowed[10].."]+") then
   if v:match("["..nallowed[10].."]+") == v then
    return true
   end
  end
 else
  --print(v:match("["..nallowed[10].."]+"),v)
  if v:match("["..nallowed[10].."]+") == v then
   return true
  end
  return false
 end
 v = v:sub(3)
 --print("v",v)
 base = nbases[bk]
 --print("base",base)
 
 if v:match("["..nallowed[base].."]+") == v then
  return true
 end
end

ifunc.number = isnumber

function isvalue(v)
 if not checkBrackets(v) then return false end
 val = pvalue(v)
 if type(val) == "table" then 
  if not val.error then
   return true
  end
 end
  return false
end

ifunc.value = isvalue

-------------------------------------------------------
--parsers

function pnumber(v) 
 v = v:lower()
 bk = v:sub(1,2):match("0(.)")
 if bk then 
  if bk:match("["..nallowed[10].."]+") then
   if v:match("["..nallowed[10].."]+") == v then
    return {type="number",raw=v,value=tonumber(v),base = 10}
   end
  end
 else
  if v:match("["..nallowed[10].."]+") == v then
   return {type="number",raw=v,value=tonumber(v),base = 10}
  end
  return {type="number",raw=v,error="not a number"}
 end
 v = v:sub(3)
 base = nbases[bk]
 
 if v:match("["..nallowed[base].."]+") == v then
  return {type="number",raw=v,value=tonumber(v,base),base = base}
 end
 return {type="number",raw=v,error="not a number"}
end

dfunc.number = pnumber

function pfunction(v)
 if not v:match("%w+%(.+") then return {error="not in function format"} end
 if not v:sub(v:len()) == ")" then return {error="no closing bracket"} end
 local name,values = v:match("(%w+)(%(.*)") --v:match("([^%(])%(([^%)])%)")
 if not name then return {error = "no name specified"} end
 if not values then return {error = "no arguments specified"} end
 local values = values:sub(2,values:len()-1)
 --sigle value
 if (not values:match(",")) and (not v:match("()")) then 
  local arg = decode(values,"value")
  return {type = "function",arguments = {arg}} 
 end
 --multiple values
 local i=1
 local args = {}
 local c = ""
 local b = ""
 local w = values
 while w do
  c,w = w:match("([^,]+),?(.*)")
  print(c,w)
  if (not c) or (c=="") then break end
  b = b..","..c
  if is("value",b:sub(2)) and checkBrackets(b) then
   args[i] = b:sub(2)
   i = i+1
   b=""
  end
 end
 print("######################################################")
 local rargs = {}
 for k,v in pairs(args) do
  print("function args",k,v)
 end
 for k,v in pairs(args) do
  rargs[k] = decode(v,"value")
 end
 return {type = "function",arguments=rargs,["function"]=name}
end

dfunc["function"] = pfunction

function pvalue(v)
 if not checkBrackets(v) then return {error="brackets are messed up"} end
 --direct number ?
 if v:sub(1,1):match("%d") then
  print(v,"couldbe a number") 
  if is("number",v) then return decode(v,"number") end
 end
 --brackets/functions
 if v:sub(v:len())==")" then
  --brackets
  if v:sub(1,1) == "(" then 
  --TODO: prevent stuff like (1*3)+(3*2) from being turned into 1*3)+(3*2
  --IDEA: c=0  ')'-> c-- '('-> c++ fail on c<0
   if checkBrackets(v:sub(2,v:len()-1)) then 
    return decode(v:sub(2,v:len()-1),"value")
   end
  else
   --function
   local f = v:match("%w+%(.*")--v:match("[^%(]%([^%)]%)")
   print("[pvalue][function]",f,v)
   --print(v:match("([^%(]+)(%(.*)"))
   if f==v then
    return decode(v,"function")
   end
  end
 end
 --signs
 for k,signp in pairs(signsp) do
  if v:match(signp) then
   local sign = signs[k]
   print("sign : "..sign)
   local b = ""
   local tv = v
   local c = ""
   --local x = ""
   local pc= false
   local r = false
   while tv do
    c,tv = tv:match("([^"..signp.."]*)"..signp.."(.*)")
    if pc == c then break end
     pc = c
    print("ds",c,tv)
    if not c then c ="" end
    b = b..c..sign
    if checkBrackets(b) then
     r=true
     --x = tv
     b = b:sub(1,b:len()-sign:len())
     break
    end
   end
   if r then
    print("---------",b,tv)
    local pre  = decode(b,"value")
    local post = decode(tv,"value")
    return {type="operation",sign=sign,pre=pre,post=post}
   end
  end 
 end
 --variables/constants
 if v:match("%a+") == v then
  return {type="placeholder",raw=v,name=v}
 end
end

dfunc.value    = pvalue
dfunc.equation = pvalue
dfunc.default  = pvalue

--tree(decode("(i*x(y))^f(y)"))
--tree(decode("1*(x*y^2)"))
--tree(decode("1+1*2^(4+1)"))
--tree(decode("f(p(0x2baDB002))"))
--tree(decode("fn(12345.67)"))
--tree(decode("f(0b1001,0xff,y(f(3.5),56),6)"))
--tree(decode("xyz"))
--tree(decode("1+1"))
--tree(decode("y=m*x+t"))
tree(decode(io.read()))
