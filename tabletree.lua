tt = {}

function tt.tree(t,off)
 if not off then off = 1 end
 for k,v in pairs(t) do
  if type(v) == "table" then
   print(string.rep("|",off)..tostring(k)..":"..type(v))
   tt.tree(v,off+1)
  else
   print(string.rep("|",off)..tostring(k)..":"..type(v).."="..tostring(v))
  end
 end
end

return tt
