EQUATION PARSER

A program that turns equations into lua tables

Files:
tabletree.lua - a library that prints tables in a human readable format
test.lua - currently this is the file where all the magic happens this will be turned into a library

--Example
local tree = require("tabletree").tree --tabletree only provides this one function
tree(decode("y=f(1+3*4,x)^0b100") --decode is provided by test.lua

produces this output:
<"some" debug output>
|post:table
||post:table
|||raw:string=0b100
|||type:string=number
|||base:number=2
|||value:number=4
||sign:string=^
||raw:string=f(1+3*4,x)^0b100
||type:string=operation
||pre:table
|||raw:string=f(1+3*4,x)
|||type:string=function
|||function:string=f
|||arguments:table
||||1:table
|||||post:table
||||||post:table
|||||||raw:string=4
|||||||type:string=number
|||||||base:number=10
|||||||value:number=4
||||||sign:string=*
||||||raw:string=3*4
||||||type:string=operation
||||||pre:table
|||||||raw:string=3
|||||||type:string=number
|||||||base:number=10
|||||||value:number=3
|||||sign:string=+
|||||raw:string=1+3*4
|||||type:string=operation
|||||pre:table
||||||raw:string=1
||||||type:string=number
||||||base:number=10
||||||value:number=1
||||2:table
|||||raw:string=x
|||||type:string=placeholder
|||||name:string=x
|sign:string==
|raw:string=y=f(1+3*4,x)^0b100
|type:string=operation
|pre:table
||raw:string=y
||type:string=placeholder
||name:string=y



which means that the table decode("y=f(1+3*4,x)^0b100") returned looks like this:

{
 type = "operation"
 sign = "="
 raw  = "y=f(1+3*4)^0b100"
 pre = {
  type = "placeholder"
  name = "y"
  raw  = "y"
 }
 post={
  type = "operation"
  sign = "^"
  raw  = "f(1+3*4,x)^0b100"
  pre = {
   type     = "function"
   function = "f"
   raw      = "f(1+3*4,x)"
   arguments = {
    1={
     type = "operation"
     sign = "+"
     raw  = "1+3*4"
     pre  = {
      type  = "number"
      value = 1
      base  = 10
      raw   = "1"
     }
     post = {
      type = "operation"
      sign = "*"
      raw  = "3*4"
      pre  = {
       type  = "number"
       value = 3
       base  = 10
       raw   = "3"
      }
      post = {
       type  = "number"
       value = 4
       base  = 10
       raw   = "4" 
      }
     }
    }
    2={
     type = "placeholder"
     name = "x"
     raw  = "x"
    }
   }
  }
  post = {
   type  = "number"
   value = 4
   base  = 2
   raw   = "0b100"
  }
 }
}

NOTE: negating values (-x) is not supprted yet